package edu.au.cc.gallery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketConfiguration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class S3Service {

	@Autowired
	private S3Client s3Client;
	private Region defaultRegion;
	
	public S3Service(@Value("${aws.s3.region}") String region) {
		this.defaultRegion = Region.of(region);
	}
	
	public void createBucket(String bucketName) {
		createBucket(bucketName, defaultRegion);
	}
	
	public void createBucket(String bucketName, Region region) {
		CreateBucketRequest request = CreateBucketRequest
				.builder()
				.bucket(bucketName)
				.createBucketConfiguration(CreateBucketConfiguration.builder()
						.locationConstraint(region.id())
						.build())
				.build();
		s3Client.createBucket(request);
	}
	
	public void putObject(String bucketName, String key, byte[] data) {
		PutObjectRequest request = PutObjectRequest
				.builder()
				.bucket(bucketName)
				.key(key)
				.acl(ObjectCannedACL.PUBLIC_READ)
				.build();
		s3Client.putObject(request, RequestBody.fromBytes(data));				
	}
	
	public void deleteObject(String bucketName, String key) {
		DeleteObjectRequest request = DeleteObjectRequest
				.builder()
				.bucket(bucketName)
				.key(key)
				.build();
		s3Client.deleteObject(request);
	}
}
