package edu.au.cc.gallery.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import edu.au.cc.gallery.model.Image;
import edu.au.cc.gallery.service.ImageService;

@Controller
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	private ImageService imageService;

	@GetMapping("/upload")
	public String showUploadView() {
		return "upload";
	}
	
	@PostMapping("/uploadExec")
	public String handleImageUpload(Authentication authentication, @RequestParam("file") MultipartFile file) {
		String user = authentication.getName();
		imageService.uploadImage(file, user);
		return "redirect:/";
	}
	
	@GetMapping("/list")
	public String showImageListView(Authentication authentication, Model model) {
		List<Image> images = imageService.getImages(authentication.getName());
		model.addAttribute("images", images);
		return "image-list";
	}
	
	@GetMapping("/{username}/{fileName}")
	public String showImageView(Authentication authentication,
			@PathVariable("username") String username,
			@PathVariable("fileName") String fileName,
			Model model) {
		if (!authentication.getName().equals(username)) {
			return "redirect:/access-denied";
		}
		Image image = imageService.getImage(username + "/" + fileName);
		model.addAttribute("image", image);
		return "image-view";
	}
	
	@GetMapping("/delete/{username}/{fileName}")
	public String deleteImage(Authentication authentication,
			@PathVariable("username") String username,
			@PathVariable("fileName") String fileName) {
		String key = username + "/" + fileName;
		imageService.deleteImage(key);
		return "redirect:/image/list";
	}
}
