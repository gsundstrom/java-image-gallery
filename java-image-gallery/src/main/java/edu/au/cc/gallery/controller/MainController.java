package edu.au.cc.gallery.controller;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

	@GetMapping("/")
	public String main(Authentication authentication, Model model) {
		model.addAttribute("isAdmin", isAdmin(authentication));
		return "main";
	}
	
	@GetMapping("/access-denied")
	public String accessDenied() {
		return "access-denied";
	}
	
	private boolean isAdmin(Authentication authentication) {
		List<GrantedAuthority> roles = (List<GrantedAuthority>) authentication.getAuthorities();
		return roles.stream().anyMatch(role -> "ROLE_ADMIN".equals(role.getAuthority()));
	}
}
