package edu.au.cc.gallery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import edu.au.cc.gallery.model.Image;
import edu.au.cc.gallery.repository.ImageRepository;

@Service
public class ImageServiceImpl implements ImageService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ImageServiceImpl.class);
	
	@Autowired
	private S3Service s3Service;
	@Autowired
	private ImageRepository imageRepository;
	@Value("${aws.s3.bucket}")
	private String bucket;

	@Override
	public void uploadImage(MultipartFile image, String username) {
		String key = generateKey(username, image.getOriginalFilename());
		try {
			byte[] data = image.getBytes();
			s3Service.putObject(bucket, key, data);
			imageRepository.save(new Image(username, key, generateUrl(key)));
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
		}
	}

	@Override
	public void deleteImage(String key) {
		s3Service.deleteObject(bucket, key);
		imageRepository.deleteById(key);
	}

	@Override
	public List<Image> getImages(String username) {
		return imageRepository.getImagesByUsername(username);
	}
	
	private String generateKey(String username, String fileName) {
		return String.format("%s/%d%s", username, System.currentTimeMillis(), fileName);
	}
	
	private String generateUrl(String key) {
		return String.format("https://%s.s3.amazonaws.com/%s", bucket, key);
	}
	
	private String extractKey(String url) {
		String[] tokens = url.split("/");
		return tokens[tokens.length - 1];
	}

	@Override
	public Image getImage(String key) {
		return imageRepository.findById(key).orElse(null);
	}

}
