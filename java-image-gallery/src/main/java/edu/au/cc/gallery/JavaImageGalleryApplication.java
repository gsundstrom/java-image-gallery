package edu.au.cc.gallery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaImageGalleryApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaImageGalleryApplication.class, args);
	}

}
