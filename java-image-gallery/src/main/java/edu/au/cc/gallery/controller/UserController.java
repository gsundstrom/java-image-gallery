package edu.au.cc.gallery.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.au.cc.gallery.model.User;
import edu.au.cc.gallery.model.UserForm;
import edu.au.cc.gallery.service.UserService;

@Controller()
@RequestMapping("admin")
public class UserController {
	
	@Autowired
	private UserService userService;

	@GetMapping()
	public String showIndex(Model model) {
		List<User> users = userService.getUsers();
		model.addAttribute("users", users);
		return "index";
	}
	
	@GetMapping("/detail/{username}")
	public String showUserDetail(@PathVariable("username") String username, Model model) {
		User user = userService.getUser(username);
		model.addAttribute("user", user);
		return "detail";
	}
	
	@GetMapping("/delete/{username}")
	public String deleteUser(@PathVariable("username") String username) {
		userService.deleteUser(username);
		return "redirect:/admin";
	}
	
	@GetMapping("/add-user-view")
	public String addUserView(Model model) {
		model.addAttribute("userForm", new UserForm());
		return "add-user";
	}
	
	@PostMapping("/add-user")
	public String addUser(@ModelAttribute UserForm userForm) {
		userService.createUser(userForm);		
		return "redirect:/admin";
	}
	
	@PostMapping("/edit-user")
	public String editUser(@ModelAttribute User user) {
		userService.editUser(user);
		return "redirect:/admin";
	}
}
