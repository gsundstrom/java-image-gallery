package edu.au.cc.gallery.constants;

public class UserAdminConstants {
	
	// MAIN MENU
	public static final String LIST_USERS_TEMPLATE = "%d)\tList Users";
	public static final String ADD_USER_TEMPLATE = "%d)\tAdd User";
	public static final String EDIT_USER_TEMPLATE = "%d)\tEdit User";
	public static final String DELETE_USER_TEMPLATE = "%d)\tDelete User";
	public static final String QUIT_TEMPLATE = "%d)\tQuit";
	public static final String COMMAND_PROMPT = "Enter Command> ";
	public static final String ILLEGAL_CHOICE_TEMPLATE = "You must choose a number between %d and %d (inclusive).";
	
	// USER LIST
	private static final String HEADER_ROW = "username   password   full name";
	private static final String DIVIDER = "-------------------------------";
	public static final String TABLE_HEADER = HEADER_ROW + "\n" + DIVIDER;
	public static final String NO_USERS = "There are currently no saved users";
	
	public static final String KEEP_CURRENT = "(press enter to keep current)";
	public static final String USERNAME_PROMPT = "Username> ";
	public static final String PASSWORD_PROMPT = "Password> ";
	public static final String NEW_PASSWORD_PROMPT = String.format("New password %s> ", KEEP_CURRENT);
	public static final String FULL_NAME_PROMPT = "Full name> ";
	public static final String NEW_FULL_NAME_PROMPT = String.format("New full name %s> ", KEEP_CURRENT);
	public static final String EDIT_PROMPT = "Username to edit> ";
	public static final String DELETE_PROMPT = "Enter username to delete> ";
	public static final String CONFIRM_DELETE_TEMPLATE = "Are you sure that you want to delete %s? ";
	public static final String ERROR_TEMPLATE = "Error: %s";
	public static final String NO_SUCH_USER = "No such user.";
	public static final String CONFIRMATION = "yes";
	public static final String DELETED = "Deleted.";
	public static final String EXIT_MESSAGE = "Bye.";

}
