package edu.au.cc.gallery.service;

import java.util.List;

import edu.au.cc.gallery.model.Authority;
import edu.au.cc.gallery.model.User;
import edu.au.cc.gallery.model.UserForm;

public interface UserService {

	List<User> getUsers();
	
	User getUser(String username);
	
	boolean userExists(String username);
	
	User createUser(UserForm userForm);
	
	User editUser(User user);
	
	User deleteUser(String username);
	
}
