package edu.au.cc.gallery.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class S3Configuration {

	@Value("${aws.s3.region}")
	private String defaultRegion;

	@Bean
	public S3Client s3Client() {
		Region region = Region.of(defaultRegion);
		return S3Client.builder().region(region).credentialsProvider(DefaultCredentialsProvider.create()).build();
	}
}
