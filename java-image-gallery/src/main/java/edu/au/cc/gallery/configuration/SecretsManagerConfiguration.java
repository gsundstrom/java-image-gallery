package edu.au.cc.gallery.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;

@Configuration
public class SecretsManagerConfiguration {
	
	@Value("${aws.s3.region}")
	private String defaultRegion;

	@Bean
	public SecretsManagerClient secretsManagerClient() {
		Region region = Region.of(defaultRegion);
		return SecretsManagerClient.builder().region(region).credentialsProvider(DefaultCredentialsProvider.create()).build();
	}
}
