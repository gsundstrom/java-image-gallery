package edu.au.cc.gallery.exception;

public class NoSuchUserException extends RuntimeException {

	public NoSuchUserException(String username) {
		super(String.format("No user with the username %s exists", username));
	}
}
