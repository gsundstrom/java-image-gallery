package edu.au.cc.gallery.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.au.cc.gallery.exception.NoSuchUserException;
import edu.au.cc.gallery.exception.UserAlreadyExistsException;
import edu.au.cc.gallery.model.Authority;
import edu.au.cc.gallery.model.User;
import edu.au.cc.gallery.model.UserForm;
import edu.au.cc.gallery.repository.AuthorityRepository;
import edu.au.cc.gallery.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	public UserRepository userRepository;
	
	@Autowired
	public AuthorityRepository authorityRepository;

	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	@Override
	public User getUser(String username) {
		Optional<User> optional = userRepository.findById(username);
		if (!optional.isPresent()) {
			throw new NoSuchUserException(username);
		}
		return optional.get();
	}
	
	@Override
	public boolean userExists(String username) {
		return userRepository.existsById(username);
	}

	@Override
	public User createUser(UserForm userForm) {
		if (userRepository.existsById(userForm.getUsername())) {
			throw new UserAlreadyExistsException(userForm.getUsername());
		}
		User user = userRepository.save(userForm.getUser());
		String role = userForm.getAdmin() ? "ROLE_ADMIN" : "ROLE_USER";
		authorityRepository.save(new Authority(userForm.getUsername(), role));
		return user;
	}

	@Override
	public User editUser(User user) {
		User currentUser = getUser(user.getUsername());
		String updatedPassword = user.getPassword().isEmpty() ? currentUser.getPassword() : user.getPassword();
		String updatedFullName = user.getFullName().isEmpty() ? currentUser.getFullName() : user.getFullName();
		user.setPassword(updatedPassword);
		user.setFullName(updatedFullName);
		return userRepository.save(user);
	}

	@Override
	public User deleteUser(String username) {
		authorityRepository.deleteById(username);
		User user = getUser(username);
		userRepository.delete(user);
		return user;
	}

}
