package edu.au.cc.gallery.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.au.cc.gallery.model.User;

public interface UserRepository extends CrudRepository<User, String>{

	@Override
	List<User> findAll();
}
