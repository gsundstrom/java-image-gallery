package edu.au.cc.gallery.repository;

import org.springframework.data.repository.CrudRepository;

import edu.au.cc.gallery.model.Authority;

public interface AuthorityRepository extends CrudRepository<Authority, String> {

}
