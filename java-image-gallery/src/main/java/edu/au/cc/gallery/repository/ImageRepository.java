package edu.au.cc.gallery.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.au.cc.gallery.model.Image;

public interface ImageRepository extends CrudRepository<Image, String> {
	
	List<Image> getImagesByUsername(String username);
	
}
