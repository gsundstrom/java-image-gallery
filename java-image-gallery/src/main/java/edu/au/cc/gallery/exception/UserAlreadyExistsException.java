package edu.au.cc.gallery.exception;

public class UserAlreadyExistsException extends RuntimeException {

	public UserAlreadyExistsException(String username) {
		super(String.format("A user with the username %s already exists", username));
	}
}
