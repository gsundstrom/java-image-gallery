package edu.au.cc.gallery.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import edu.au.cc.gallery.model.Image;

public interface ImageService {

	void uploadImage(MultipartFile image, String user);
	
	void deleteImage(String imageUrl);
	
	List<Image> getImages(String username);
	
	Image getImage(String key);
	
}
