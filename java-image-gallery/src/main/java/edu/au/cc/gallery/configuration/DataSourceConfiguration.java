package edu.au.cc.gallery.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.au.cc.gallery.model.DatabaseSecret;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;

@Configuration
public class DataSourceConfiguration {
	
	private static final String URL_PREFIX = "jdbc:postgresql://";

	@Autowired SecretsManagerClient secretsManagerClient;
	@Autowired ObjectMapper objectMapper;
	
	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;

	@Bean
	public DataSource dataSource() throws JsonMappingException, JsonProcessingException {
		GetSecretValueRequest request = GetSecretValueRequest.builder().secretId("sec-ig-image_gallery").build();
		GetSecretValueResponse response = secretsManagerClient.getSecretValue(request);
		DatabaseSecret dbSecret = objectMapper.readValue(response.secretString(), DatabaseSecret.class);
		
		String dbUrl = !url.isEmpty() ? url : String.format("%s%s:%d/%s", URL_PREFIX, dbSecret.getHost(), dbSecret.getPort(), dbSecret.getDatabase());
		String dbUsername = !username.isEmpty() ? username : dbSecret.getUsername();
		String dbPassword = !password.isEmpty() ? password : dbSecret.getPassword();
		
		return DataSourceBuilder.create()
				.url(dbUrl)
				.username(dbUsername)
				.password(dbPassword)
				.driverClassName("org.postgresql.Driver")
				.build();
	}
}
